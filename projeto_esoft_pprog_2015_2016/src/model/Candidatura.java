/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author joao Carvalho
 * @author Dinis Rosas
 */
public class Candidatura {
    private String nomeEmpresa;
    private String morada;
    private String telemovel;
    private String convites;
    private String produtos;
    private int fae;
    Object CentroExposicao;


    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public void setTelemovel(String telemovel) {
        this.telemovel = telemovel;
    }

    public void setConvites(String convites) {
        this.convites = convites;
    }

    public void setProdutos(String produtos) {
        this.produtos = produtos;
    }

    public boolean valida() {
        // Introduzir as validações aqui
        return true;
    }

    @Override
    public String toString() {
        String sTxt;

        sTxt = String.format("%s;\n%s;\n%s;\n%s;\n%s;\n\n", this.nomeEmpresa, this.morada,
                this.telemovel, this.convites, this.produtos);

        return sTxt;
    }
}
