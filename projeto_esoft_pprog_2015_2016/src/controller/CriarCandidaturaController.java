/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Exposicao;
import model.Candidatura;

/**
 * ESOFT - 2015/2016
 * 
 * @author Dinis Rosas
 * @author João Carvalho 
 * 
 */
public class CriarCandidaturaController {

    private final Exposicao m_oEXP;
    private Candidatura m_candidatura;
    
    public CriarCandidaturaController(Exposicao exp) {
        this.m_oEXP = exp;
    }
    
    public void novaCandidatura()
    {
        this.m_candidatura = this.m_oEXP.novaCandidatura();
    }
    
    public void setDados(String nomeEmpresa, String morada, String telemovel, String convites, String produtos2)
    {
        this.m_candidatura.setNomeEmpresa(nomeEmpresa);
        this.m_candidatura.setMorada(morada);
        this.m_candidatura.setTelemovel(telemovel);
        this.m_candidatura.setConvites(convites);
        this.m_candidatura.setProdutos(produtos2);
    }

    public boolean registaCandidatura() {
        return this.m_oEXP.registaCandidatura(m_candidatura);
    }

    public String getCandidaturaString() {
        return this.m_candidatura.toString();
    }

    
}
