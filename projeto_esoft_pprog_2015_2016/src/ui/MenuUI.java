/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import model.CentroExposicoes;
import model.Utilizador;
import utils.Utils;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Joao Carvalho
 */
public class MenuUI {

    private final CentroExposicoes m_ce;
    private String op;

    public MenuUI(CentroExposicoes ce) {
        m_ce = ce;
    }

    public void run() throws IOException {
        //Uma espécie de Login
        List<Utilizador> lUsers = this.m_ce.getUtilizadores();
        Utilizador utilizador;
        do {
            utilizador = (Utilizador) Utils.apresentaESeleciona(lUsers, "Selecione Utilizador");
        } while (utilizador == null);

        do {
            //opcao = "1";
            System.out.println("\n### Projeto PPROG-ESOFT 2015/2016 | it2 ###");
            System.out.println("\t1. Criar Exposição");
            System.out.println("\t2. Definir FAE");
            System.out.println("\t3. Atribuir Candidatura");
            System.out.println("\t4. Decidir Candidaturas");
            System.out.println("\t5. Registar Candidatura");
            System.out.println("\t6. Registar Utilizador");
            System.out.println("\t7. Confirmar Registo de Utilizador");
            System.out.println("\t8. Criar Demsonstração da Exposição");
            System.out.println("\t9. Definir Recursos");
            System.out.println("\t0. Sair");
            System.out.println("### Projeto PPROG-ESOFT 2015/2016 | it2 ###");

            op = Utils.readLineFromConsole("Opção Pretendida: ");

            switch (op) {
                case "1":
                    CriarExposicaoUI ui1 = new CriarExposicaoUI(m_ce, (Utilizador) utilizador);
                    ui1.run();
                    break;
                case "2":
                    DefinirFAEUI ui2 = new DefinirFAEUI(m_ce);
                    ui2.run();
                    break;
                case "3":
                    break;
                case "4":
                    DecidirCandidaturasUI ui4 = new DecidirCandidaturasUI(m_ce, utilizador);
                    ui4.run();
                    break;
                case "6":
                    RegistarUtilizadorUI ui6 = new RegistarUtilizadorUI(m_ce, utilizador);
                    ui6.run();
                    break;
                case "7":
                    ConfirmarRegistoUtilizadorUI ui7 = new ConfirmarRegistoUtilizadorUI(m_ce, utilizador);
                    ui7.run();
                    break;
                case "8":
                    break;
                case "9":
                    break;
                case "0":
                    System.out.println("Aplicação Terminada");
                    break;
                default:
                    System.out.println("Opção Inválida. Tente Novamente");
            }
        } while (!op.equals("0"));
    }
}
