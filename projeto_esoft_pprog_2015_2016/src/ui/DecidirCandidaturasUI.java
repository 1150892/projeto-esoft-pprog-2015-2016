/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import model.Decisao;
import controller.DecidirCandidaturasController;
import model.CentroExposicoes;
import model.DecidirCandidaturas;
import model.Exposicao;
import model.Utilizador;
import utils.Utils;
import static utils.Utils.apresentaLista;
import static utils.Utils.selecionaObject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DecidirCandidaturasUI
{
    private final DecidirCandidaturasController m_oController;
    public final Utilizador m_oUu;
    private List<Exposicao> lExposicoes; 
    private List<Decisao> lDecisoes;
    
    public DecidirCandidaturasUI(CentroExposicoes ce, Utilizador u)
    {
        this.m_oController = new DecidirCandidaturasController(ce);
        this.m_oUu=u;
    }
    
    public void run()
    {
        System.out.println("\nDecidir Candidaturas:");
        
        
        List<Exposicao> lExposicoes = m_oController.getListaExposicoesDoFAE(m_oUu);
        Object objExposicao;
        objExposicao=Utils.apresentaESeleciona(lExposicoes, "Selecione Exposição");
        
        lDecisoes=m_oController.selecionaExposicao((Exposicao) objExposicao,m_oUu);
        Object objDecisao;
        objDecisao=Utils.apresentaESeleciona(lDecisoes, "Selecione Candidatura");
        
        System.out.println("Info da Candidatura:\n" + 
                m_oController.getInformacaoDaCandidaturaPorDecidir((Decisao) objDecisao));
        
        // Introduz Justificação
        Boolean s_dec;
        String s_textoJustificacao;
        s_dec = Utils.confirma("Aceita(S) ou Rejeita(N) Candidatura?");
        s_textoJustificacao =Utils.readLineFromConsole("introduz Texto Justificativo");
        
        m_oController.setDecisao(s_dec, s_textoJustificacao); 
        
        m_oController.registaDecisao();
         
    }      
}
  
