/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import model.CentroExposicoes;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            CentroExposicoes ce = new CentroExposicoes();
            MenuUI uiMenu = new MenuUI(ce);
            uiMenu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
