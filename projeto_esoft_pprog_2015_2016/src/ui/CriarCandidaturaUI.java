/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;


import controller.CriarCandidaturaController;
import model.Exposicao;
import utils.Utils;

/**
 * ESOFT - 2015/2016
 * 
 * @author Dinis Rosas
 * @author João Carvalho 
 * 
 */
public class CriarCandidaturaUI {
    
    private final CriarCandidaturaController m_oController;
    
    public CriarCandidaturaUI(Exposicao m_exp) {
        
        this.m_oController = new CriarCandidaturaController(m_exp);
        
    }

    

    public void run() {
        System.out.println("\n### Criar Candidatura ###");
        
        m_oController.novaCandidatura();

        introduzDados();
        
        apresentaDados();

        if (Utils.confirma("Confirma os dados da Candidatura? (S/N)")) {
            
            if (m_oController.registaCandidatura()) {
                System.out.println("Candidatura criada com sucesso.");
            } else {
                System.out.println("Não foi possivel guardar corretamente a Candidatura.");
            }
        }
    }

    private void introduzDados() {
        String nomeEmpresa  = Utils.readLineFromConsole("Introduza nome da empresa: ");
        String morada = Utils.readLineFromConsole("Introduza morada: ");
        String telemovel = Utils.readLineFromConsole("Introduza telemovel: ");
        String convites = Utils.readLineFromConsole("Introduza convites: ");
        
        String produtos = Utils.readLineFromConsole("Intruduza produtos: ");
       
         m_oController.setDados(nomeEmpresa, morada, telemovel, convites, produtos);
    }       

    private void apresentaDados() {
        System.out.println("\nCandidatura:\n" + m_oController.getCandidaturaString());
    }

    
}
